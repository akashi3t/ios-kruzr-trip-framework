Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '13.0'
s.name = "KruzrTripFramework"
s.summary = "KruzrTripFramework manages trips."
s.requires_arc = true

# 2
s.version = "1.3.3"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Akash Deep" => "akash@kruzr.co" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://gitlab.com/akashi3t/ios-kruzr-trip-framework.git"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://gitlab.com/akashi3t/ios-kruzr-trip-framework.git", :branch => 'master'}

# 7
s.framework = "UIKit"
s.dependency "KruzrDriveDetectionFramework"
s.dependency "Alamofire"
s.dependency "AWSS3"
s.dependency "SSZipArchive"
s.dependency "CSV.swift"
#s.framework = "CoreData"


# 8
s.resource_bundles = {'KruzrTripFramework' => ['KruzrTripFramework/*.xcdatamodeld']}

# 9

s.ios.vendored_frameworks = 'KruzrTripFramework.framework'
# 10
s.swift_version = "4.2"

end
